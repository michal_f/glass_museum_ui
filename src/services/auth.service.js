import axios from 'axios';

const API_URL = "http://localhost:3000/admin/loginTest";


class AuthService {
    login(username, password) {
        console.log({
            login: username,
            password: password
        })
        return axios
            .post(API_URL + "", {
                login: username,
                password: password
            })
            .then(response => {
                console.log(response)
                if (response.data.accessToken) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                }
                return response.data;
            });
    }

    logout() {
        localStorage.removeItem("user");
    }

    getCurrentUser() {
        return JSON.parse(localStorage.getItem('user'));;
    }
}

export default new AuthService();
