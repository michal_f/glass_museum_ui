import React from 'react';
import Login from "./components/login.component";

function App() {
  return (
    <div className="App">
    <h1>React app</h1>
    <Login />
    </div>
  );
}

export default App;
